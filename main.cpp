#include <iostream>
#include <csignal>
#include <cstdlib>
#include <boost/asio.hpp>

void signalHandler(int signum)
{
    std::cout << "Interrupt signal (" << signum << ")";

    exit(signum);
}

void handle_dns_error(const boost::system::error_code& error)
{
    //Reschedule next connect time, etc. 
    std::cout << error.message() << std::endl;
}

int main()
{
    boost::asio::io_context io_context;
    boost::system::error_code error;
    boost::asio::ip::tcp::resolver resolver(io_context);

    std::string hostname = "o11y.love";

    const char *tmp = std::getenv("DNS_HOSTNAME");
    std::string host_env(tmp ? tmp : "");

    std::cout << "DNS_HOSTNAME: " << host_env << std::endl;

    if (!host_env.empty())
        hostname = host_env;

    signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);  

    while (true) {
        // pre-allocated receive buffer
        char *rcvbuf  = new char[1024*1024]; 

        boost::asio::ip::tcp::resolver::results_type results = resolver.resolve(hostname, "443", error);

        // DNS resolution failed or has empty results, handle the error
        if (error != boost::system::errc::success || results.empty()) {
            handle_dns_error(error);
            continue;
        }

        // Print DNS results for debugging
        std::cout << "Resolve host '" << hostname << "', got " << results.size() << " results." << std::endl;
        for(boost::asio::ip::tcp::endpoint const& endpoint : results) {
            std::cout << endpoint << "\n";
        }

        //Connect to the server and use the rcv buffer
        //NOTE: Left out for demo reasons

        //Free the receive buffer
        delete[] rcvbuf;

        // Sleep until the next request
        sleep(1);
    }

    return 0;
}
