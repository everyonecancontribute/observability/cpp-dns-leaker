# https://hub.docker.com/_/gcc 
FROM debian:buster AS builder

ENV DEBIAN_FRONTEND noninteractive

RUN set -ex; apt-get update && apt-get install -y g++ cmake libboost-dev libboost-system-dev libboost-thread-dev libasio-dev && rm -rf /var/lib/apt/lists/*

COPY . /usr/src/app

RUN set -ex; cd /usr/src/app && cmake . && make && make install 

FROM debian:buster AS runtime

RUN set -ex; apt-get update && apt-get install -y libboost-system1.67.0 libboost-thread1.67.0 libboost-chrono1.67.0 libboost-date-time1.67.0 libboost-atomic1.67.0 && rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/local/bin /usr/local/bin

ENTRYPOINT ["cpp-dns-leaker"]



