# cpp-dns-leaker

> _Note_: This application is now part of the general [k8s-o11y-chaos](https://gitlab.com/everyonecancontribute/observability/k8s-o11y-chaos) project, that brings together many chaos engineering practices. 

Demo application which tries to resolve the DNS hostname `o11y.love`, and leaks memory when resolution fails.

- Continuous DNS resolving to simulate a network service which does health pings.
- Leak memory (1MB buffer) when DNS resolving fails, and continue with requests.

The memory leak bug is intentional to simulate a programming mistake, causing memory leaks in production because of DNS failure.

## Background

The demo is written in C++, leaning into a real world problem [@dnsmichi](https://twitter.com/dnsmichi) needed to debug as OSS maintainer in 2016-2019. Back then, there was no deployment observability or cloud native chaos engineering to trigger this error before released to the customers.

The story is part of these talks:

- "Left Shift your SLOs" at [SLOConf 2021](https://www.youtube.com/watch?v=VwZrliyHbMs&feature=emb_title)
- "From Monitoring to Observability: Left Shift your SLOs with Chaos" at [All Day DevOps 2021](https://docs.google.com/presentation/d/1nTpngPtzoOgUu74b1ibDhmxYTwAocxn0/edit#slide=id.p1)
- "Left Shift your SLOs with Chaos" at [SLOConf 2022](https://sloconf.com)
- "From Monitoring to Observability: Left Shift your SLOs with Chaos" at [Chaos Carnival 2022](https://docs.google.com/presentation/d/1FgoMAlaFOCQbM2yW6tVpyf7WmLNtaFoaQulJl6k20tQ/edit#slide=id.g10b8d016450_1_0), [KubeCon EU 2022](https://docs.google.com/presentation/d/14BUwSaHub-EGw-CQmdfW0F_HxxT9-qrF-MjiXQbrkUc/edit#slide=id.p)


[![KubeCon EU 2022: From Monitoring to Observability: Left Shift your SLOs with Chaos](https://img.youtube.com/vi/BkREMg8adaI/0.jpg)](https://www.youtube.com/watch?v=BkREMg8adaI)

## Day 2 Ops - simulate DNS failure

The problem can be simulated in production by e.g. using Chaos Engineering with an experiment to break DNS resolution.

Error cases:

- DNS resolution fails and the TCP resolver returns a system error code.
- DNS resolution is successful but does not provide result records.

## Development

### Local Source

Install boost, cmake, gcc with Homebrew on macOS.

```
$ brew install cmake boost
```

Create a new build directly, generate the CMake configuration inside, and run `make` to build the binary.

```
mkdir -p build
cd build
cmake ..
cd ..

make -C build
```

for later code changes to compile, only run `make -C build`. 

Run the binary. Optionally you can override the DNS hostname using the `DNS_HOSTNAME` variable.

```
$ ./build/cpp-dns-leaker
$ DNS_HOSTNAME=gitlab.com ./build/cpp-dns-leaker
```

### Docker

Docker image with builder and runtime steps.

```
$ docker build . -t cpp-dns-leaker
```

Run the container image. Optionally specificy the `DNS_HOSTNAME` environment variable. 

```
$ docker run -ti cpp-dns-leaker:latest

$ docker run -ti -e DNS_HOSTNAME=gitlab.com cpp-dns-leaker:latest
```

### Kubernetes 

Create a Kubernetes cluster, e.g. in Civo Cloud. Follow [this guide](https://www.civo.com/learn/kubernetes-cluster-administration-using-civo-cli) to setup the API token and CLI.

```
$ civo kubernetes create kubeconeu

$ civo kubernetes config kubeconeu --save

$ kubectl config use-context kubeconeu
$ kubectl get node
```

Apply the deployment configuration.

```
$ kubectl apply -f manifests/cpp-dns-leaker-service.yml                                                                                                          ─╯
deployment.apps/cpp-dns-leaker-service-o11y created
deployment.apps/cpp-dns-leaker-service-cncf created
deployment.apps/cpp-dns-leaker-service-gitlab created
```

Inspect the pods and follow the logs to see the DNS resolution.

```
$ kubectl get pods                                                                                                                                               ─╯
NAME                                             READY   STATUS    RESTARTS   AGE
cpp-dns-leaker-service-o11y-55d9498b7f-h5wnp     1/1     Running   0          23s
cpp-dns-leaker-service-cncf-75d756dcdf-khq26     1/1     Running   0          23s
cpp-dns-leaker-service-o11y-55d9498b7f-v2kkm     1/1     Running   0          23s
cpp-dns-leaker-service-gitlab-5ddf58f457-77gjf   1/1     Running   0          23s

$ kubectl logs -f cpp-dns-leaker-service-gitlab-5ddf58f457-77gjf
```

## Demo

Use the Kubernetes cluster with the deployed application.

In order to demonstrate the memory leaks, a few preparations need to be done.

- [x] Install [kube-prometheus](https://prometheus-operator.dev/docs/prologue/quick-start/) into the cluster.
- [ ] Install Chaos into the cluster, either Litmus or Chaos Mesh
  - Install [Litmus Chaos](https://docs.litmuschaos.io/docs/getting-started/installation) into the cluster.
  - [x] Install [Chaos Mesh](https://chaos-mesh.org/docs/simulate-dns-chaos-on-kubernetes/) into the cluster.

### Prometheus Observability

Install kube-prometheus and run a PromQL query in the frontend to monitor the container memory usage.

```
git clone https://github.com/prometheus-operator/kube-prometheus.git
cd kube-prometheus
kubectl create -f manifests/setup
until kubectl get servicemonitors --all-namespaces ; do date; sleep 1; echo ""; done
kubectl create -f manifests/

kubectl --namespace monitoring port-forward svc/prometheus-k8s 9090
```

PromQL query
```
container_memory_rss{container=~"cpp-dns-leaker-service.*"}
```

### Chaos Platform

#### Litmus

```
kubectl apply -f https://litmuschaos.github.io/litmus/2.8.0/litmus-2.8.0.yaml

kubectl get pods -n litmus

kubectl --namespace litmus port-forward svc/litmusportal-frontend-service 9091
```

TODO: Figure out why the self-service agent doesn't register itself.

#### Chaos Mesh

Install Chaos Mesh as Chaos Engineering Platform.

https://chaos-mesh.org/docs/production-installation-using-helm/

Follow https://github.com/civo/kubernetes-marketplace/blob/master/chaos-mesh/install.sh to apply Civo specific installation parameters for k3s. 

```
helm repo add chaos-mesh https://charts.chaos-mesh.org

helm search repo chaos-mesh

kubectl create ns chaos-testing

helm install chaos-mesh chaos-mesh/chaos-mesh -n=chaos-testing --set chaosDaemon.runtime=containerd --set chaosDaemon.socketPath=/run/k3s/containerd/containerd.sock --version 2.2.0

# Disable RBAC token auth: https://chaos-mesh.org/docs/manage-user-permissions/#enable-or-disable-permission-authentication
helm upgrade chaos-mesh chaos-mesh/chaos-mesh --namespace=chaos-testing --version 2.2.0 --set dashboard.securityMode=false

kubectl --namespace chaos-testing port-forward svc/chaos-dashboard 2333
```


### Actions

- [ ] Deploy an alert rule for Prometheus to trigger memory usage
- [ ] Define and run the DNS chaos experiment
  - Litmus: [Pod Dns Error](https://litmuschaos.github.io/litmus/experiments/categories/pods/pod-dns-error/).
  - Chaos Mesh: [DNS Faults](https://chaos-mesh.org/docs/simulate-dns-chaos-on-kubernetes/)

#### Chaos Mesh

Chaos Mesh: Enable DNS server, disable dashboard auth, enable host network, runtime containerd
```
helm upgrade chaos-mesh chaos-mesh/chaos-mesh \
--namespace=chaos-testing \
--version 2.2.0 \
--set dnsServer.create=true \
--set dashboard.create=true \
--set dashboard.securityMode=false \
--set chaosDaemon.hostNetwork=true \
--set chaosDaemon.runtime=containerd \
--set chaosDaemon.socketPath=/run/k3s/containerd/containerd.sock


kubectl get pods -n chaos-testing -l app.kubernetes.io/component=chaos-dns-server
```

Create DNSChaos and Schedule CRDs.

```
vim manifests/chaos-mesh/dnschaos.yaml

apiVersion: chaos-mesh.org/v1alpha1
kind: DNSChaos
metadata:
  name: dns-chaos-kubecon
  namespace: chaos-testing
spec:
  action: error # important: error, no random responses
  mode: all
  patterns:
    - o11y.*
    - cncf.io
    - gitlab.com
  selector:
    namespaces:
      # Target the cpp-dns-leaker pods
      - default
  duration: '5h'

---
apiVersion: chaos-mesh.org/v1alpha1
kind: Schedule
metadata:
  name: schedule-dns-chaos-kubecon
spec:
  schedule: '* * * * *'
  historyLimit: 2
  concurrencyPolicy: 'Allow'
  type: 'DNSChaos'
  dnsChaos:
    action: error # important: error, no random responses
    mode: all
    patterns:
      - o11y.*
      - cncf.io
      - gitlab.com
    selector:
      namespaces:
        - default
    duration: '60s'


kubectl apply -f manifests/chaos-mesh/dnschaos.yaml
```




### Additional Tasks

- Install Keptn as quality gate, and run the chaos experiments in a staging environment.
- Install the [GitLab Agent for Kubernetes]() to manage the project, and update the deployments.
- Connect with GitLab MRs.




